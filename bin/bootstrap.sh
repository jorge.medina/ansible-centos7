#!/bin/bash

#
# script: bootstrap.sh
# author: jorge.medina@kronops.com.mx
# desc: Installs ansible on redhat from epel repo & prepares the environment for infrastructure deployments.

# Enable debug mode and log to file
export DEBUG=1
LOG=bootstrap.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# vars

# main
echo
echo "Installing basic requirements."
sudo yum -y install vim-enhanced curl git openssh openssh-clients
echo

echo "Installing basic requirements."
sudo yum -y install epel-release
sudo yum -y check-update

echo
echo "Installing Ansible dependencies."
sudo yum -y install python python-pip PyYAML python-jinja2 python2-paramiko python2-crypto \
	python-passlib sshpass ansible
sudo yum -y update ansible
echo

echo "Creating and changing ownership and permissions for /var/lib/ansible."
sudo mkdir -p /var/lib/ansible/retries
sudo chown -R root:root /var/lib/ansible
sudo chmod 770 /var/lib/ansible/retries

echo "Changing ansible ownership and permissions for /var/log/ansible"
sudo mkdir -p /var/log/ansible
sudo chown root:root /var/log/ansible
sudo chmod 770 /var/log/ansible
sudo touch /var/log/ansible/ansible.log
sudo chown root:root /var/log/ansible/ansible.log
sudo chmod 660 /var/log/ansible/ansible.log

