# ansible-centos7

Código para demostración de ansible para administrar las configuraciones de centos7.

## Vagrant

Usamos vagrant para crear un par de máquinas virtuales con centos7 en donde podamos
practicar nuestro trabajo sin afectar nuestro equipo personal o un servidor productivo.

## Shell

Usamos scripts en shell para instalar paqueteria necesaria para usar ansible.

## Ansible

Usamos ansible para automatizar rutinas en los servidores creados por vagrant.

